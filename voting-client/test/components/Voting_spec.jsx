import {Voting} from '../../src/components/Voting';
import React from 'react';
import ReactDOM from 'react-dom';
import {
	renderIntoDocument,
	scryRenderedDOMComponentsWithTag,
	Simulate
} from 'react-addons-test-utils';
import {List} from 'immutable';
import {expect} from 'chai';

describe('Voting', () => {

	it('renders a pair of buttons', () => {
		const component = renderIntoDocument(
			<Voting pair={['HelloPanda', 'Movie2']} />	
		);
		//find button elements in componet
		const buttons = scryRenderedDOMComponentsWithTag(component, 'button');
		expect(buttons.length).to.equal(2);
		expect(buttons[0].textContent).to.equal('HelloPanda');
		expect(buttons[1].textContent).to.equal('Movie2');
	});

	it('invokes callback when button is clicked', () => {
		let votedWith;
		const vote = (entry) => votedWith = entry;

		const component = renderIntoDocument(
			<Voting pair={['Movie1', 'Movie2']}
					vote={vote}/>
		);
		const buttons = scryRenderedDOMComponentsWithTag(component, 'button');
		Simulate.click(buttons[0]);
		expect(votedWith).to.equal('Movie1');
	});

	it('disables buttons when user has voted', () => {
		const component = renderIntoDocument(
			<Voting pair = {['Movie1', 'Movie2']}
					hasVoted = 'Movie1' />
		);

		const buttons = scryRenderedDOMComponentsWithTag(component, 'button');
		expect(buttons[0].textContent).to.contain('Voted');
	});

	it('renders just the winner when there is one', () => {
		const component = renderIntoDocument(
			<Voting winner = 'Movie1'
					pair = {['Movie1', 'Movie2']} />
		);
		const buttons = scryRenderedDOMComponentsWithTag(component, 'button');
		expect(buttons.length).to.equal(0);

		const winner = ReactDOM.findDOMNode(component.refs.winner);
		expect(winner).to.be.ok;
		expect(winner.textContent).to.contain('Movie1');
	});

	it('renders as a pure component', () => {
		const pair = ['Movie1', 'Movie2'];
		const container = document.createElement('div');
		let component = ReactDOM.render(
			<Voting pair = {pair} />,
			container
		);
		let firstButton = scryRenderedDOMComponentsWithTag(component, 'button')[0];
		expect(firstButton.textContent).to.equal('Movie1');

		pair[0] = 'Movie3';
		component = ReactDOM.render(
			<Voting pair={pair}/>,
			container
		);
		firstButton = scryRenderedDOMComponentsWithTag(component, 'button')[0];
		expect(firstButton.textContent).to.equal('Movie1');
	});

	it('does update DOM when prop changes', () => {
		const pair = List.of('Movie1', 'Movie2');
		const container = document.createElement('div');
		let component = ReactDOM.render(
			<Voting pair={pair} />,
			container
		);

		let firstButton = scryRenderedDOMComponentsWithTag(component, 'button')[0];
		expect(firstButton.textContent).to.equal('Movie1');

		const newPair = pair.set(0, 'Movie3');
		component = ReactDOM.render(
			<Voting pair = {newPair} />,
			container
		);
		firstButton = scryRenderedDOMComponentsWithTag(component, 'button')[0];
		expect(firstButton.textContent).to.equal('Movie3');
	});
});