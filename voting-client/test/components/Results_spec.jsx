import React from 'react';
import ReactDOM from 'react-dom';
import {
	renderIntoDocument,
	scryRenderedDOMComponentsWithClass,
	Simulate
} from 'react-addons-test-utils';
import {List, Map} from 'immutable';
import {Results} from '../../src/components/Results';
import {expect} from 'chai';

describe('Results', () => {

	it('renders entries with vote counts or zero', () => {
		const pair = List.of('Movie1', 'Movie2');
		const tally = Map({'Movie1': 5});
		const component = renderIntoDocument(
			<Results pair={pair} tally={tally}/>
		);
		const entries = scryRenderedDOMComponentsWithClass(component, 'entry');
		const [m1, m2] = entries.map(e => e.textContent);

		expect(entries.length).to.equal(2);
		expect(m1).to.contain('Movie1');
		expect(m1).to.contain('5');
		expect(m2).to.contain('Movie2');
		expect(m2).to.contain('0');
	});

	it('invokes the next cb when next button is clicked', () => {
		let nextInvoked = false;
		const next = () => nextInvoked = true;

		const pair = List.of('Movie1', 'Movie2');
		const component = renderIntoDocument(
			<Results pair={pair}
					 tally ={Map()}
					 next = {next}/>
		);
		Simulate.click(ReactDOM.findDOMNode(component.refs.next));
		//was next called (this.props.next) if it was nextInvoked =true
		expect(nextInvoked).to.equal(true);
	});

	it('renders the winner when there is one', () => {
		const component = renderIntoDocument(
			<Results winner='Movie1'
					 pair={['Movie1', 'Movie2']}
					 tally={Map()} />
		);
		const winner = ReactDOM.findDOMNode(component.refs.winner);
		expect(winner).to.be.ok;
		expect(winner.textContent).to.contain('Movie1');
	});


























});