import {List, Map, fromJS} from 'immutable';
import {expect} from 'chai';

import reducer from '../src/reducer';

describe('reducer', () => {

	it('handles SET_STATE', () => {
		const initialState = Map();
		const action = {
			type: 'SET_STATE',
			state: Map({
				vote: Map({
					pair: List.of('Movie1', 'Movie2'),
					tally: Map({Movie1: 1})
				})
			})
		};
		const nextState = reducer(initialState, action);

		expect(nextState).to.equal(fromJS({
			vote: {
				pair: ['Movie1', 'Movie2'],
				tally: {
					Movie1: 1
				}
			}
		}));
	});

	it('handles SET_STATE with plain JS payload', () => {
		const initialState = Map();
		const action = {
			type: 'SET_STATE',
			state: {
				vote: {
					pair: ['Movie1', 'Movie2'],
					tally: {Movie1: 1}
				}
			}
		};
		const nextState = reducer(initialState, action);

		expect(nextState).to.be.equal(fromJS({
			vote: {
				pair: ['Movie1', 'Movie2'],
				tally: {Movie1: 1}
			}
		}));
	});

	it('handles SET_STATE without an initial state', () => {
		const action = {
			type: 'SET_STATE',
			state: {
				vote: {
					pair: ['Movie1', 'Movie2'],
					tally: {Movie1: 1}
				}
			}
		};
		const nextState = reducer(undefined, action);

		expect(nextState).to.be.equal(fromJS({
			vote: {
				pair: ['Movie1', 'Movie2'],
				tally: {Movie1: 1}
			}
		}));
	});

	it('handles VOTE by setting hasVoted', () => {
		const state = fromJS({
			vote: {
				pair: ['Movie1', 'Movie2'],
				tally: {Movie1: 1}
			}
		});
		const action = {type: 'VOTE', entry: 'Movie1'};
		const nextState = reducer(state, action);

		expect(nextState).to.be.equal(fromJS({
			vote: {
				pair: ['Movie1', 'Movie2'],
				tally: {Movie1: 1}
			},
			hasVoted: 'Movie1'
		}));
	});

	it('does not set hasVoted for VOTE on invalid entry', () => {
		const state = fromJS({
			vote: {
				pair: ['Movie1', 'Movie2'],
				tally: {Movie1:1}
			}
		});
		const action = {type:'VOTE', entry: 'Movie3'};
		const nextState = reducer(state, action);

		expect(nextState).to.equal(fromJS({
			vote: {
				pair: ['Movie1', 'Movie2'],
				tally: {Movie1: 1}
			}
		}));
	});

	it('removes hasVoted on SET_STATE if pair changes', () => {
		const initialState = fromJS({
			vote:{
				pair: ['Movie1', 'Movie2'],
				tally: {Movie1: 1}
			},
			hasVoted: 'Movie1'
		});
		const action = {
			type:'SET_STATE',
			state: {
				vote: {
					pair: ['Movie3', 'Movie4']
				}			
			}
		};
		const nextState = reducer(initialState, action);

		expect(nextState).to.equal(fromJS({
			vote: {
				pair: ['Movie3', 'Movie4']
			}
		}));
	});












});