
import {List, Map} from 'immutable';
import {expect} from 'chai';

import {setEntries, next, vote} from '../src/core';

// we wanna describe what our app does
describe('application logic', () => {
// first we have a function called setEntries & we wanna describe its specs
	describe('setEntries', () => {
		//setEntries is a function that takes in 2 params (state, entries) to return a 'new' state
		//we test to see if new state is what we expect it to be
		it('adds the entries to the state', () => {
			const state = Map();
			const entries = List.of('Movie1', 'Movie2');
			const nextState = setEntries(state, entries);
			expect(nextState).to.equal(Map({
				entries: List.of('Movie1', 'Movie2')
			}));
		});

		it('converts to immutable', () => {
			const state = Map();
			const entries = ['Movie1', 'Movie2'];
			const nextState = setEntries(state, entries);
			expect(nextState).to.equal(Map({
				entries: List.of('Movie1', 'Movie2')
			}));
		});
	});

	//describes the functionality of next
	describe('next', () => {
		it('takes the next two entries under vote', () => {
			const state = Map({
				entries: List.of('Movie1', 'Movie2', 'Movie3')
			});
			const nextState = next(state);
			expect(nextState).to.equal(Map({
				vote: Map({
					pair: List.of('Movie1', 'Movie2')
				}),
				entries: List.of('Movie3')
			}));
		});
// case where there are odd number of entries how?
		it('puts winner of the current vote back to the entries', () => {
			const state = Map({
				vote: Map({
					pair: List.of('Movie1', 'Movie2'),
					tally: Map({
						'Movie1': 2,
						'Movie2': 3
					})
				}),
				entries: List.of('Movie3', 'Movie4')
			});
			const nextState = next(state);
			expect(nextState).to.equal(Map({
				vote: Map({
					pair: List.of('Movie3', 'Movie4')
				}),
				entries: List.of('Movie2')
			}));
		});
// case where there are odd number of entries how?
		it('puts both if votes on both entries are tied', () => {
			const state = Map({
				vote: Map({
					pair: List.of('Movie1', 'Movie2'),
					tally: Map({
						'Movie1': 2,
						'Movie2': 2
					})
				}),
				entries: List.of('Movie3', 'Movie4')
			});
			const nextState = next(state);
			expect(nextState).to.equal(Map({
				vote: Map({
					pair: List.of('Movie3', 'Movie4')
				}),
				entries: List.of('Movie1', 'Movie2')
			}));
		});

		it('marks winner when just one entry left', () => {
			const state = Map({
				vote: Map({
					pair: List.of('Movie3', 'Movie4'),
					tally: Map({
						'Movie3': 3,
						'Movie4': 4
					})
				}),
				entries: List()
			});
			const nextState = next(state);
			expect(nextState).to.equal(Map({
				winner: 'Movie4'
			}));
		});
	});

	describe('vote', () => {
		
		it('creates a tally for the voted entry if there is none', () => {
			const state = Map({
					pair: List.of('Movie1', 'Movie2')
			});

			const nextState = vote(state, 'Movie1');
			expect(nextState).to.equal(Map ({
				pair: List.of('Movie1', 'Movie2'),
				tally: Map({
				'Movie1': 1
				})
			}));
		});

		it ('increments tally by 1 if there is already one', () => {
			const state = Map({
				pair: List.of('Movie1', 'Movie2'),
				tally: Map({
					'Movie1': 3,
					'Movie2': 2
				})
			});

			const nextState = vote(state, 'Movie2');
			expect(nextState).to.equal(Map({
				pair:List.of('Movie1', 'Movie2'),
				tally: Map({
					'Movie1': 3,
					'Movie2': 3
				})
			}));
		});
	});	
});


/*plainObjectFunc = function (pairArray, tallyCount1, tallyCount2, entryArray ) {
	vote: {
		pair: pairArray,
		tally: {
			'Movie1': tallyCount1,
			'Movie2': tallyCount2
		}
	},
	entries: entryArray
};

// this will convert my plain JS objs and arrs to Immutable Maps & Lists
fromJS(plainObjectFunc(['Movie1', 'Movie2'], 1, 3, []));*/