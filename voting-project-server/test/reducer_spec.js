// test for the reducer function 

import {List, Map, fromJS} from 'immutable';
import {expect} from 'chai';

import reducer from '../src/reducer';

describe('reducer', () => {

	it('handles SET_ENTRIES', () => {
		const initialState = Map();
		const action = {type: 'SET_ENTRIES', entries: ['Movie1', 'Movie2']};
		const nextState = reducer(initialState, action);

		expect(nextState).to.be.equal(fromJS({entries: ['Movie1', 'Movie2']}));
	});

	it('handles NEXT', () => {
		const initialState = fromJS({entries: ['Movie1', 'Movie2']});
		const action = {type: 'NEXT'};
		const nextState = reducer(initialState, action);

		expect(nextState).to.equal(fromJS({
			vote: {
				pair: ['Movie1', 'Movie2']
			},
			entries: []
		}));
	});

	it('handles VOTE', ()=> {
		const initialState = fromJS({
			vote : {
				pair: ['Movie1', 'Movie2'],
				tally: {
					'Movie1': 1,
					'Movie2': 1
				}
			},
			entries: []
		});
		const action = {type: 'VOTE', entry: 'Movie2'};
		const nextState = reducer(initialState, action);
		expect(nextState).to.be.equal(fromJS({
			vote: {
				pair: ['Movie1', 'Movie2'],
				tally: {
					'Movie1': 1,
					'Movie2': 2
				}
			},
			entries: []
		}));
	});

	it('has an initial state', () => {
		const action = {type: 'SET_ENTRIES', entries: ['movie1']};
		// reducer already set inital state
		const nextState = reducer(undefined, action);

		expect(nextState).to.be.equal(fromJS({
			entries: ['movie1']
		}));
	});

	it('can be used with reduce', () => {
		const actions = [
			{type: 'SET_ENTRIES', entries: ['Movie1', 'Movie2']},
			{type: 'NEXT'},
			{type: 'VOTE', entry: 'Movie1'},
			{type: 'VOTE', entry: 'Movie2'},
			{type: 'VOTE', entry: 'Movie1'},
		];
		const currentState = actions.reduce(reducer, Map());

		expect(currentState).to.equal(fromJS({
			vote: {
				pair: ['Movie1', 'Movie2'],
				tally: {
					'Movie1': 2,
					'Movie2': 1
				}
			},
			entries: []
		}));
	});
});