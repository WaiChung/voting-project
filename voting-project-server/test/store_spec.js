import {Map, fromJS} from 'immutable';
import {expect} from 'chai';

import makeStore from '../src/store';

describe('store', () => {

	it('is a Redux store configured with the correct reducer', () => {
		const store = makeStore();
		expect(store.getState()).to.be.equal(Map());

		store.dispatch({
			type: 'SET_ENTRIES',
			entries: ['Movie1', 'Movie2']
		});
		expect(store.getState()).to.be.equal(fromJS({
			entries: ['Movie1', 'Movie2']
		}));
	});
});