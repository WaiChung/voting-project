/* 
1. Client sends action to the server
2. Server hands the action to the Redux store
3. Store calls reducer and logic related to that action is executed
4. Store updates its state based on the return value of the reducer
5. Store executes the listener function subscribed by the server
6. Server emits a state event
7. All connected clients will received the new state
*/

import Server from 'socket.io';

export default function startServer(store) {
	const io = new Server().attach(8091);

	//subscribe a listener to the store (server & store interaction)
	store.subscribe(
		// listener gets the current state turn it into JS object and emits it on the Socket.io server as a 'state' event
		() => io.emit('state', store.getState().toJS())
	);

	//listen to 'connection' events, send current state to the clients when they connect (server & client interaction)
	io.on('connection', (socket) => {
		socket.emit('state', store.getState().toJS());
		//when clients dispatch 'action' events into the Redux store 
		socket.on('action', store.dispatch.bind(store));
	});
}