import {createStore} from 'redux';
import reducer from './reducer';

export default function makeStore() {
	//store will use reducer to apply actions to the current state as well as store the resultant next state
	return createStore(reducer);
}