//import the functions form core
import {setEntries, next, vote, INITIAL_STATE} from './core';

export default function reducer(state = INITIAL_STATE, action) {
	// Holds multiple function, figure out which one to call and call it 

	switch(action.type) {
		case 'SET_ENTRIES':
		return setEntries(state, action.entries);
		case 'VOTE': 
		//takes in the current branch of the full state of the program & the action returns an updated state (voteState)
		// finds the branch of the state to update with the updated state
		return state.update('vote', voteState => vote(voteState, action.entry));
		case 'NEXT':
		return next(state);
	}
	// if reducer does not recognize the action, it will just return the state
	return state;
}