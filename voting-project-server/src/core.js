import {List, Map} from 'immutable';

function getWinners(vote) {
	if(!vote) return [];
	const [firstContestant, secContestant] = vote.get('pair');
	const firstContestantVotes = vote.getIn(
		//follow this path, if there is no firstContestant, create one in the place
		['tally', firstContestant], 
		// if value missing, init with 0 votes
		0
	);
	const secContestantVotes = vote.getIn(
		['tally', secContestant],
		0
	);
	if (firstContestantVotes > secContestantVotes) {
		return [firstContestant];
	} else if (secContestantVotes > firstContestantVotes) {
		return [secContestant];
	} else {
		return [firstContestant, secContestant];
	}
}

export function setEntries(state, entries) {
	return state.set('entries', List(entries));
}

export function next(state) {
	const entries = state.get('entries').concat(getWinners(state.get('vote')));
	if(entries.size === 1) {
		//good idea to morph the old state in to new one instead of build new state from scratch
		//for future proofing: where unrelated data in state should pass through function unchanged
		return state.remove('vote')
					.remove('entries')
					.set('winner', entries.first());
	}
	return state.merge({
		vote: Map({pair: entries.take(2)}),
		entries: entries.skip(2)
	});
}
export function vote(voteState, entry) {
	// the function state.updateIn reaches into a nested D-struct path and applies itself there
	return voteState.updateIn(
			//path: if there are keys missing along path, create new Maps in their place
			['tally', entry],
			// if the value at the end of path is missing, init with 0.
			0,
			// update the tally value of the chosen entry with +1
			tally => tally +1
		);
}

export const INITIAL_STATE = Map();