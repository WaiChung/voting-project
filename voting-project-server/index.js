import makeStore from './src/store';
import startServer from './src/server';

export const store = makeStore();
//integrating Socket.io server and Redux state container
startServer(store);

store.dispatch({
	type: 'SET_ENTRIES',
	entries: require('./entries.json')
});

store.dispatch({type: 'NEXT'});